# Data Analysis Advanced Jupyter Docker Stack

This image build on top of [´jupyter/tensorflow-notebook´](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-tensorflow-notebook) and includes all packages installed there. Ecpecially those installed in the [´jupyter/scipy-notebook´](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-scipy-notebook).
Additionally the following packages are installed:

- ROOT
- uproot
- vector
- awkward
- lxml
- pytables
- requests-ftp

## Lectures

The following lectures are using this docker images in their exercises:

- Moderne Methoden der Datenanalyse (MDMA)
- Teilchenphysik 1 (TP 1)

## Test the image

To test the image run

```bash
docker pull kitphysics/data-analysis-advanced:latest
```

to pull the image and

```bash
docker run -it -p 8888:8888 kitphysics/data-analysis-advanced:latest
```

to run it. Press on the link in the output to open JupyterLab in your browser.
