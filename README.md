# Jupyter Docker Container for teaching

This repo contains all the docker images containing Jupyter which are used for teaching at the faculty of physics.
They build on top of the [official Jupyter Docker images](https://github.com/jupyter/docker-stacks).

These images can either be used directly on a PC or with JupyterHub, e.g. from the SCC.
For more information how to start the container yourself you can visit the [official Jupyter Docker stacks docs](https://jupyter-docker-stacks.readthedocs.io/en/latest/).

## Lectures

The following lectures are using this docker images in their exercises.

| Lecture                                  | Needed Packages                                                                                                                                   | Image                                                                                 |
| ---------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------- |
| CGDA                                     | <ul><li>numpy</li> <li>scipy</li> <li>matplotlib</li> <li>pandas</li> <li>kafe2</li></ul>                                                         | data-analysis-basic                                                                   |
| Praktikum 1&2                            | <ul><li>numpy</li> <li>scipy</li> <li>matplotlib</li> <li>pandas</li> <li>kafe2</li> <li>PhyPraKit</li> <li>nbconvert(LaTeX export)</li></ul>     | data-analysis-basic                                                                   |
| Moderne Methoden der Datenanalyse (MDMA) | <ul><li>ROOT</li> <li>uproot</li> <li>tensorflow</li> <li>vector</li> <li>awkward</li> <li>lxml</li> <li>requests-ftp</li> <li>pytables</li></ul> | data-analysis-advanced                                                                |
| TP1                                      | <ul><li>ROOT</li> <li>uproot</li> <li>vector</li> <li>awkward</li> <li>pytables</li> <li>Geant4</li> <li>Herwig</li></ul>                         | <ul><li>data-analysis-advanced</li> <li>geant4-notebook</li> <li>herwig-notebook</li> |