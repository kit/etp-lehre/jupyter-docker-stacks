# Data Analysis Advanced Jupyter Docker Stack

This image build on top of [´jupyter/scipy-notebook´](https://jupyter-docker-stacks.readthedocs.io/en/latest/using/selecting.html#jupyter-scipy-notebook) and includes all packages installed there. Additionally, the following packages are installed:

- kafe2
- PhyPraKit

## Lectures

The following lectures are using this docker images in their exercises:

- Praktikum 1&2
- CGDA

## Test the image

To test the image run

```bash
docker pull kitphysics/data-analysis-basic:latest
```

to pull the image and

```bash
docker run -it -p 8888:8888 kitphysics/data-analysis-basic:latest
```

to run it. Press on the link in the output to open JupyterLab in your browser.
